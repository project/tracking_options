<?php

use Drupal\Core\Render\BubbleableMetadata;
/**
 * Implements hook_theme().
 */
function tracking_options_theme($existing, $type, $theme, $path) {
  return [
    'tracking_options_wrapper' => [
      'render element' => 'element',
    ],
  ];
}

/**
 * Implements hook_page_attachments_alter().
 */
function tracking_options_page_attachments_alter(array &$attachments) {
  $attachments['#attached']['library'][] = 'tracking_options/tracking-options-js';

  $to_disable = \Drupal::service('tracking_options.manager')
    ->getDisablingProviders();

  if (empty($to_disable)) {
    return;
  }

  $attachments['#attached']['library'][] = 'tracking_options/tracking-options-base';
  $attachments['#attached']['drupalSettings']['defaultTracking'] = \Drupal::service('tracking_options.manager')->getDefaultTrackingConfig();

  $disabled_scripts = [];

  foreach ($attachments['#attached']['html_head'] as $key => $element) {
    if (isset($element[1]) && array_key_exists($element[1], $to_disable)) {
      $disabled_scripts[] = [
        'type' => $element[1],
        'code' => $element[0]['#value'],
      ];
      unset($attachments['#attached']['html_head'][$key]);
    }
  }

  $load_disabled_scripts = '';

  if (!empty($disabled_scripts)) {
    foreach ($disabled_scripts as $key => $script) {

      $code_snippet = $script['code']->jsonSerialize();

      // make sure there's no multiline JS code
      $code_snippet = str_replace(["\n", "\r"], '', $code_snippet);
      // replace any single quotes, prevent possible JS errors
      $code_snippet = str_replace("'", '"', $code_snippet);

      $load_disabled_scripts .= 'var scriptTag = document.createElement("script");';
      $load_disabled_scripts .= 'scriptTag.text = ';
      $load_disabled_scripts .= "'" . $code_snippet . "';";
      $load_disabled_scripts .= 'document.body.appendChild(scriptTag);';
    }

    if (!empty($load_disabled_scripts)) {
      $attachments['#attached']['html_head'][] = [
        [
          '#type' => 'html_tag',
          '#tag' => 'script',
          '#value' => 'function trackingOptionsLoadScripts() {' . $load_disabled_scripts . '}',
        ],
        'tracking-options-js',
      ];
    }
  }
}

/**
 * Implements hook_token_info().
 */
function tracking_options_token_info() {
  $tokens['tracking-options'] = [
    'name' => t('Tracking Options token'),
    'description' => t('Display Tracking Options form using token'),
  ];
  return [
    'tokens' => ['node' => $tokens],
  ];
}

/**
 * Implements hook_tokens().
 */
function tracking_options_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      if ($name === 'tracking-options') {
        $form = \Drupal::formBuilder()
          ->getForm('Drupal\tracking_options\Form\TrackingOptionsForm');
        $replacements[$original] = \Drupal::service('renderer')->render($form);
      }
    }
  }

  return $replacements;
}
