<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * Configuration
 * Developers
 * Special thanks


INTRODUCTION
------------

Your users have the right for privacy.

Analytic services like Matomo and Google Analytics drop a Cookie, which is (at least in the EU) forbidden as a default behavior

"Cookies or similar devices must not be used unless the subscriber or user of the relevant terminal equipment:
 (a) is provided with clear and comprehensive information about the purposes of the storage of, or access to, that information; and
 (b) has given his or her consent."

You can use this module on top of:

 * [Google Analytics](https://www.drupal.org/project/google_analytics)
 * [Matomo](https://www.drupal.org/project/matomo)
 * ...


FEATURES
------------

 * prevent loading the analytic scripts by default
 * use the browser storage instead of an cookie for the user setting
 * opt out for matomo (if you host you service by yourself it should still be ok to have just an opt out (ask your layer of trust for a clear statement!)
 * a token AND/OR a block to place the checkbox and translatable text somewhere on your site
 * with this module we respect the Do-Not-Track Browser setting by default! Info will be shown instead of the checkbox if a user has this setting aktive in his browser


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configuration page is located here: admin/config/tracking-options/settings
 * Select the providers you wish to enable or disable tracking for
 * Choose if you want your visitors to be tracked or not by default (Opted-in or Opted-out)
 * Activate opt/out form using 'Tracking Options block', or you can use Token [node:tracking-options].
   If you're using token inside Text fields - probably you will need the [Token Filter](https://www.drupal.org/project/token_filter) module installed & `Token Filter` enabled for the `Text Format` where it should be used (admin/config/content/formats)


DEVELOPERS
--------------
If you want to set the value dynamically (e.g. from a button in a cookie dialog) the following JS commands are available:

| Command                                        | Description                      |
| ---------------------------------------------- |:--------------------------------:|
| `Drupal.behaviors.trackingOptions.doNotTrack`   | `true` if "Do not track" is set  |
| `Drupal.behaviors.trackingOptions.enabled`      | `true` if tracking is enabled    |
| `Drupal.behaviors.trackingOptions.toggle()`     | Toggles the tracking behaviour   |
| `Drupal.behaviors.trackingOptions.set(true)`    | Enable tracking                  |
| `Drupal.behaviors.trackingOptions.set(false)`   | Disable tracking                 |


SPECIAL THANKS
--------------
thx to [Jon Uhlmann](https://twitter.com/Jonnitto) for his work over at [dev.to](https://dev.to/jonnitto/matamo-with-no-iframes-31ej) after we had a discussion about this topic :)


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
