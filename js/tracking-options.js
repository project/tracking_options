(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.trackingOptions = {
    attach: function () {
      $(once('tracking-options', 'body')).each(function () {
        if (typeof trackingOptionsLoadScripts == 'function' && trackingOptout.enabled) {
          trackingOptionsLoadScripts();
        }
      });
    },
    defaultTracking: function () {
      return drupalSettings.defaultTracking;
    },
    // API methods
    doNotTrack: function () {
      return trackingOptout.doNotTrack;
    },
    enabled: function () {
      return trackingOptout.enabled;
    },
    toggle: function () {
      return trackingOptout.toggle();
    },
    set: function (value) {
      return trackingOptout.set(value);
    },
  };
}(jQuery, Drupal, drupalSettings, once));
