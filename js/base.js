/**
 * @author Jon Uhlmann
 * https://dev.to/jonnitto/matamo-with-no-iframes-31ej
 */

(function($) {
  /**
   * Set the options for trackingOptout:
   * `doNotTrackRespected`: Set whether you have your installation respect the browserâ€™s DoNotTrack option
   * `storageKey`: Set the name of the key where the current status get's saved
   * `enabledPerDefault`: Set this to true if you want the tracking enabled per default
   */
  const OPTIONS = {
    doNotTrackRespected: true,
    storageKey: 'trackingEnabled',
    enabledPerDefault: Drupal.behaviors.trackingOptions.defaultTracking()
  };

  function isMSExternal(external) {
    return 'msTrackingProtectionEnabled' in external;
  }

  /**
   * get the browserâ€™s DoNotTrack setting
   * @var DO_NOT_TRACK boolean
   */
  const DO_NOT_TRACK = (() => {
    const dnt =
        window.doNotTrack || navigator.doNotTrack || navigator.msDoNotTrack;
    if (dnt === '1' || dnt === 'yes') {
      return true;
    }
    // All versions of IE
    if (window.external && isMSExternal(window.external)) {
      return window.external.msTrackingProtectionEnabled();
    }
    return false;
  })();

  /**
   * Return the string for select elements
   * @param {string} element
   * @param {string} modifier
   */
  function selector(element, modifier) {
    return `.tracking-options${element ? `__${element}` : ''}${
        modifier ? `--${modifier}` : ''
    }`;
  }

  /**
   * Return an array from nodes
   * @param selector
   * @param context
   */
// eslint-disable-next-line no-shadow
  function nodeArray(selector) {
    return [].slice.call(document.querySelectorAll(selector));
  }

  /**
   * Save all checkboxes in an array
   */
  const CHECKBOXES = nodeArray(selector('input'));

  /**
   * Set visibility of elements
   * @param {string} selector
   * @param {boolean} show
   */
// eslint-disable-next-line no-shadow
  function visibility(selector, show) {
    nodeArray(selector).forEach(element => {
      // eslint-disable-next-line no-param-reassign
      element.style.display = show ? null : 'none';
    });
  }

  /**
   * Remove elements from the dom
   * @param {string} selector
   */
// eslint-disable-next-line no-shadow
  function remove(selector) {
    nodeArray(selector).forEach(element => {
      element.parentNode.removeChild(element);
    });
  }

  /**
   * Hide elements
   * @param {string} selector
   */
// eslint-disable-next-line no-shadow
  function hide(selector) {
    visibility(selector, false);
  }

  /**
   * Show elements
   * @param {string} selector
   */
// eslint-disable-next-line no-shadow
  function show(selector) {
    visibility(selector, true);
  }

  /**
   * Display the current status of the tracking (enabled or not)
   * @param {bool} enable
   */
  function displayStatus(enable) {
    CHECKBOXES.forEach(checkbox => {
      // eslint-disable-next-line no-param-reassign
      checkbox.checked = enable;
    });
    // eslint-disable-next-line no-use-before-define
    trackingOptout.enabled = enable;
    hide(selector('status'));
    show(selector('status', enable ? 'active' : 'inactive'));
  }

  /**
   * Set Status
   * @param {bool} enable
   */
  function setStatus(enable) {
    localStorage.setItem(OPTIONS.storageKey, enable ? 'true' : 'false');
    displayStatus(!!enable);
    return !!enable;
  }

  /**
   * Toggle the status
   */
  function toggleStatus() {
    // eslint-disable-next-line no-use-before-define
    const status = !trackingOptout.enabled;
    setStatus(status);
    return status;
  }

  /**
   * Log if do not track is active and toggle or set is called
   */
  function forbidden() {
    console.log('Do not track is enabled, so no tracking where enabled');
    return false;
  }

  const trackingOptout = {
    doNotTrack: DO_NOT_TRACK,
    enabled: null,
    toggle: toggleStatus,
    set: setStatus,
  };

  (() => {
    const OLDIE = selector('oldie');
    const DNT = selector('dnt');
    const LABEL = selector('label');

    if (DO_NOT_TRACK && OPTIONS.doNotTrackRespected) {
      remove(OLDIE);
      show(DNT);
      trackingOptout.enabled = false;
      trackingOptout.set = forbidden;
      trackingOptout.toggle = forbidden;
      return;
    }
    remove(DNT);
    if (typeof Storage !== 'undefined') {
      remove(OLDIE);
      const SAVED_STATUS = localStorage.getItem(OPTIONS.storageKey);
      if (SAVED_STATUS === null) {
        // Set default value
        // setStatus(OPTIONS.enabledPerDefault);
        displayStatus(OPTIONS.enabledPerDefault);
      }
      else {
        displayStatus(SAVED_STATUS === 'true');
      }
      CHECKBOXES.forEach(input => {
        input.addEventListener('change', toggleStatus);
      });
      show(LABEL);
      return;
    }
    remove(LABEL);
    show(OLDIE);
  })();

  window.trackingOptout = trackingOptout;

})();
