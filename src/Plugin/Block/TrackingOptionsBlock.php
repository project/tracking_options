<?php

namespace Drupal\tracking_options\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Tracking Options' Block.
 *
 * @Block(
 *   id = "tracking_options_block",
 *   admin_label = @Translation("Tracking Options block"),
 *   category = @Translation("tracking_options"),
 * )
 */
class TrackingOptionsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return \Drupal::formBuilder()
      ->getForm('Drupal\tracking_options\Form\TrackingOptionsForm');
  }

}
