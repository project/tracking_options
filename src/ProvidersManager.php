<?php

namespace Drupal\tracking_options;

use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\Container\ContainerInterface;

class ProvidersManager {

  /**
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ProvidersManager constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAllProviders() {
    return [
      'matomo_tracking_script' => 'Matomo',
      'google_analytics_tracking_script' => 'Google Analytics',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDisablingProviders() {
    $config = $this->configFactory->get('tracking_options.settings');
    return !empty($config->get('providers')) ? array_filter($config->get('providers')) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultTrackingConfig() {
    $config = $this->configFactory->get('tracking_options.settings');
    return $config->get('default');
  }

}
