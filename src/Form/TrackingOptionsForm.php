<?php

namespace Drupal\tracking_options\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class TrackingOptionsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  protected $providersManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->providersManager = $container->get('tracking_options.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'block_tracking_options_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!empty($this->providersManager->getDisablingProviders())) {
      $form['options_checkbox'] = [
        '#type' => 'checkbox',
        '#attributes' => ['class' => ['tracking-options__input']],
        '#theme_wrappers' => ['tracking_options_wrapper'],
      ];

      return $form;
    }

    $form["#markup"] = $this->t("Tracking options settings are only visible if at least one tracking provider is active.");
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
