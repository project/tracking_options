<?php

namespace Drupal\tracking_options\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $providersManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->providersManager = $container->get('tracking_options.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tracking_options.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracking_options_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tracking_options.settings');
    $providers = $this->providersManager->getAllProviders();
    $form['providers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Providers'),
      '#options' => $providers,
      '#description' => $this->t('Enable Tracking Options feature for specific providers from the list'),
      '#default_value' => (array) $config->get('providers'),
    ];

    foreach ($providers as $key => $provider) {
      $states[] = ':input[name="providers[' . $key . ']"]';
    }

    $form['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Tracking enabled by default?'),
      '#description' => $this->t('Choose if you want your visitors to be tracked by default (Opted-in)'),
      '#default_value' => $config->get('default'),
    ];

    if (!empty($states)) {
      $form['default']['#states'] = [
        'visible' => [
          implode(', ', $states) => ['checked' => TRUE],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('tracking_options.settings')
      ->set('providers', $form_state->getValue('providers'))
      ->set('default', $form_state->getValue('default'))
      ->save();
  }

}
